# Build a REST API in Golang by
# https://www.soberkoder.com/go-rest-api-gorilla-mux/

docker run -p 8011:8080 -e SWAGGER_JSON=/example.yaml -v C:\go\src\gitlab.com\marek2222\api\go-rest-api-gorilla-mux\swagger.yml:/example.yaml swaggerapi/swagger-ui:v3.52.3
-p porty
-e envinronment variable 
-v volume


Running & Testing the App
Finally, we are done with all the APIs, and it’s time take them for a spin. To run the app, navigate to your project directory, and run the following commands:

go build go-orders-api
./go-orders-api

/*----------------------------*/
Create Order
curl -H 'Content-Type: application/json' -d '{"orderedAt":"2019-11-09T21:21:46+00:00","customerName":"Tom Jerry","items":[{"itemId":"123","description":"IPhone 10X","quantity":1}]}' -X POST http://localhost:8080/orders

/*----------------------------*/
Get Orders
curl http://localhost:8080/orders

/*----------------------------*/
Update Order
curl -H 'Content-Type: application/json' -d '{"orderId":"1","orderedAt":"2019-11-09T21:21:46+00:00","items":[{"itemId":"123","description":"IPhone 10X","quantity":3}]}' -X PUT http://localhost:8080/orders/1

/*----------------------------*/
Delete Order
curl -X DELETE http://localhost:8080/orders/1

You can also use tools like Postman to try these requests out.

